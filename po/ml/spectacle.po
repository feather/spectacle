# Malayalam translations for spectacle package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the spectacle package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: spectacle\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-26 00:44+0000\n"
"PO-Revision-Date: 2019-12-12 21:06+0000\n"
"Last-Translator: Vivek KJ Pazhedath <vivekkj2004@gmail.com>\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: desktop/MigrateShortcuts.cpp:72 src/ShortcutActions.cpp:33
#, kde-format
msgid "Launch Spectacle"
msgstr ""

#: desktop/MigrateShortcuts.cpp:77 src/ShortcutActions.cpp:39
#, kde-format
msgid "Capture Entire Desktop"
msgstr ""

#: desktop/MigrateShortcuts.cpp:82 src/ShortcutActions.cpp:45
#, kde-format
msgid "Capture Current Monitor"
msgstr ""

#: desktop/MigrateShortcuts.cpp:87 src/ShortcutActions.cpp:51
#, kde-format
msgid "Capture Active Window"
msgstr ""

#: desktop/MigrateShortcuts.cpp:92 src/ShortcutActions.cpp:57
#, kde-format
msgid "Capture Rectangular Region"
msgstr ""

#: src/CaptureModeModel.cpp:95
#, kde-format
msgid "Rectangular Region"
msgstr ""

#: src/CaptureModeModel.cpp:102
#, kde-format
msgid "All Screens"
msgstr ""

#: src/CaptureModeModel.cpp:102
#, kde-format
msgid "Full Screen"
msgstr ""

#: src/CaptureModeModel.cpp:109
#, kde-format
msgid "All Screens (Scaled to same size)"
msgstr ""

#: src/CaptureModeModel.cpp:116
#, kde-format
msgid "Current Screen"
msgstr ""

#: src/CaptureModeModel.cpp:123
#, kde-format
msgid "Active Window"
msgstr ""

#: src/CaptureModeModel.cpp:130
#, kde-format
msgid "Window Under Cursor"
msgstr ""

#: src/CommandLineOptions.h:20
#, kde-format
msgid "Capture the entire desktop (default)"
msgstr ""

#: src/CommandLineOptions.h:24
#, kde-format
msgid "Capture the current monitor"
msgstr ""

#: src/CommandLineOptions.h:28
#, kde-format
msgid "Capture the active window"
msgstr ""

#: src/CommandLineOptions.h:32
#, kde-format
msgid ""
"Capture the window currently under the cursor, including parents of pop-up "
"menus"
msgstr ""

#: src/CommandLineOptions.h:36
#, kde-format
msgid ""
"Capture the window currently under the cursor, excluding parents of pop-up "
"menus"
msgstr ""

#: src/CommandLineOptions.h:40
#, kde-format
msgid "Capture a rectangular region of the screen"
msgstr ""

#: src/CommandLineOptions.h:44
#, kde-format
msgid "Launch Spectacle without taking a screenshot"
msgstr ""

#: src/CommandLineOptions.h:48
#, kde-format
msgid "Start in GUI mode (default)"
msgstr ""

#: src/CommandLineOptions.h:52
#, kde-format
msgid "Take a screenshot and exit without showing the GUI"
msgstr ""

#: src/CommandLineOptions.h:56
#, kde-format
msgid "Start in DBus-Activation mode"
msgstr ""

#: src/CommandLineOptions.h:60
#, kde-format
msgid ""
"In background mode, do not pop up a notification when the screenshot is taken"
msgstr ""

#: src/CommandLineOptions.h:64
#, kde-format
msgid "In background mode, save image to specified file"
msgstr ""

#: src/CommandLineOptions.h:69
#, kde-format
msgid "In background mode, delay before taking the shot (in milliseconds)"
msgstr ""

#: src/CommandLineOptions.h:74
#, kde-format
msgid ""
"In background mode, copy screenshot image to clipboard, unless -o is also "
"used."
msgstr ""

#: src/CommandLineOptions.h:78
#, kde-format
msgid "In background mode, copy screenshot file path to clipboard"
msgstr ""

#: src/CommandLineOptions.h:82
#, kde-format
msgid "Wait for a click before taking screenshot. Invalidates delay"
msgstr ""

#: src/CommandLineOptions.h:86
#, kde-format
msgid "Starts a new GUI instance of spectacle without registering to DBus"
msgstr ""

#: src/CommandLineOptions.h:90
#, kde-format
msgid "In background mode, include pointer in the screenshot"
msgstr ""

#: src/CommandLineOptions.h:94
#, kde-format
msgid "In background mode, exclude decorations in the screenshot"
msgstr ""

#: src/CommandLineOptions.h:98
#, kde-format
msgid "Open and edit existing screenshot file"
msgstr ""

#: src/ExportManager.cpp:326
#, kde-format
msgid "QImageWriter cannot write image: %1"
msgstr ""

#: src/ExportManager.cpp:341
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Cannot save screenshot because creating the directory failed:<nl/><filename>"
"%1</filename>"
msgstr ""

#: src/ExportManager.cpp:351
#, kde-format
msgid "Cannot save screenshot. Error while writing file."
msgstr ""

#: src/ExportManager.cpp:371
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Cannot save screenshot because creating the remote directory failed:<nl/"
"><filename>%1</filename>"
msgstr ""

#: src/ExportManager.cpp:382 src/ExportManager.cpp:432
#, kde-format
msgid "Cannot save screenshot. Error while writing temporary local file."
msgstr ""

#: src/ExportManager.cpp:390
#, kde-format
msgid "Unable to save image. Could not upload file to remote location."
msgstr ""

#: src/ExportManager.cpp:439
#, kde-format
msgid "Cannot save screenshot. The save filename is invalid."
msgstr ""

#: src/ExportManager.cpp:487
#, kde-format
msgid "Cannot save an empty screenshot image."
msgstr ""

#: src/ExportManager.cpp:582
#, kde-format
msgid "Printing failed. The printer failed to initialize."
msgstr ""

#: src/ExportManager.cpp:598
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Year (4 digit)"
msgstr ""

#: src/ExportManager.cpp:599
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Year (2 digit)"
msgstr ""

#: src/ExportManager.cpp:600
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Month"
msgstr ""

#: src/ExportManager.cpp:601
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Day"
msgstr ""

#: src/ExportManager.cpp:602
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Hour"
msgstr ""

#: src/ExportManager.cpp:603
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Minute"
msgstr ""

#: src/ExportManager.cpp:604
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Second"
msgstr ""

#: src/ExportManager.cpp:605
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Timezone"
msgstr ""

#: src/ExportManager.cpp:606
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Window Title"
msgstr ""

#: src/ExportManager.cpp:607
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Sequential numbering"
msgstr ""

#: src/ExportManager.cpp:609
#, kde-format
msgctxt ""
"A placeholder in the user configurable filename will replaced by the "
"specified value"
msgid "Sequential numbering, padded out to N digits"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:54
#, kde-format
msgid "Stroke:"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:86
#, kde-format
msgid "Stroke Width"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:102
#, kde-format
msgid "Stroke Color"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:140
#, kde-format
msgid "Fill:"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:152
#, kde-format
msgid "Fill Color"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:193
#, kde-format
msgid "Font:"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:221
#, kde-format
msgid "Font Color"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:262
#, kde-format
msgid "Number:"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:282
#, kde-format
msgid "Number for number annotations"
msgstr ""

#: src/Gui/AnnotationOptionsToolBarContents.qml:307
#, kde-format
msgid "Shadow"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:65
#, kde-format
msgid "None"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:72
#, kde-format
msgid "Select"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:80
#, kde-format
msgid "Freehand"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:87
#, kde-format
msgid "Highlighter"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:94
#, kde-format
msgid "Line"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:101
#, kde-format
msgid "Arrow"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:108
#, kde-format
msgid "Rectangle"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:115
#, kde-format
msgid "Ellipse"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:122
#, kde-format
msgid "Pixelate"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:129
#, kde-format
msgid "Blur"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:136
#, kde-format
msgid "Text"
msgstr ""

#: src/Gui/AnnotationsToolBarContents.qml:143
#, kde-format
msgid "Number"
msgstr ""

#: src/Gui/CaptureModeButtonsColumn.qml:31 src/Gui/MainToolBarContents.qml:97
#, kde-format
msgid "Cancel (%1 second)"
msgid_plural "Cancel (%1 seconds)"
msgstr[0] ""
msgstr[1] ""

#: src/Gui/CaptureOptions.qml:19 src/Gui/DialogPage.qml:52
#, kde-format
msgid "Take a new screenshot"
msgstr ""

#: src/Gui/CaptureOptions.qml:41 src/Gui/DialogPage.qml:113
#: src/Gui/OptionsMenu.cpp:63
#, kde-format
msgid "Capture Settings"
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:16 src/Gui/OptionsMenu.cpp:67
#: src/Gui/RecordOptions.qml:42
#, kde-format
msgid "Include mouse pointer"
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:17
#, kde-format
msgid "Show the mouse cursor in the screenshot image."
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:25 src/Gui/OptionsMenu.cpp:79
#, kde-format
msgid "Include window titlebar and borders"
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:26
#, kde-format
msgid ""
"Show the window title bar and border when taking a screenshot of a window."
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:34 src/Gui/OptionsMenu.cpp:91
#, kde-format
msgid "Capture the current pop-up only"
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:36
#, kde-format
msgid ""
"Capture only the current pop-up window (like a menu, tooltip etc) when "
"taking a screenshot of a window. If disabled, the pop-up is captured along "
"with the parent window."
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:44 src/Gui/OptionsMenu.cpp:105
#, kde-format
msgid "Quit after manual Save or Copy"
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:45
#, kde-format
msgid "Quit Spectacle after manually saving or copying the image."
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:54
#, kde-format
msgid "Capture on click"
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:56
#, kde-format
msgid "Wait for a mouse click before capturing the screenshot image."
msgstr ""

#: src/Gui/CaptureSettingsColumn.qml:66 src/Gui/OptionsMenu.cpp:135
#, kde-format
msgid "Delay:"
msgstr ""

#: src/Gui/CopiedMessage.qml:13
#, kde-format
msgid "The screenshot has been copied to the clipboard."
msgstr ""

#: src/Gui/DelaySpinBox.qml:17
#, kde-format
msgctxt "0 second delay"
msgid "No Delay"
msgstr ""

#: src/Gui/DelaySpinBox.qml:26 src/Gui/SmartSpinBox.cpp:30
#, kde-format
msgctxt "Integer number of seconds"
msgid " second"
msgid_plural " seconds"
msgstr[0] ""
msgstr[1] ""

#: src/Gui/DelaySpinBox.qml:28 src/Gui/SmartSpinBox.cpp:32
#, kde-format
msgctxt "Decimal number of seconds"
msgid " seconds"
msgstr ""

#: src/Gui/DialogPage.qml:96
#, kde-format
msgid "Capture Modes"
msgstr ""

#: src/Gui/DialogPage.qml:127
#, kde-format
msgid "Configure Spectacle…"
msgstr ""

#: src/Gui/DialogPage.qml:133 src/Gui/MainToolBarContents.qml:137
#, kde-format
msgid "Help"
msgstr ""

#: src/Gui/ExportMenu.cpp:45
#, kde-format
msgid "Open Default Screenshots Folder"
msgstr ""

#: src/Gui/ExportMenu.cpp:108
#, kde-format
msgid "Other Application..."
msgstr ""

#: src/Gui/ExportMenu.cpp:142
#, kde-format
msgid "Share"
msgstr ""

#: src/Gui/ImageView.qml:148
#, kde-format
msgid "Screenshot"
msgstr ""

#: src/Gui/ImageView.qml:152
#, kde-format
msgid "Recording"
msgstr ""

#: src/Gui/ImageView.qml:215
#, kde-format
msgid "Zoom:"
msgstr ""

#: src/Gui/ImageView.qml:229
#, kde-format
msgid "Image Zoom"
msgstr ""

#: src/Gui/MainToolBarContents.qml:55
#: src/Gui/SettingsDialog/SettingsDialog.cpp:26
#, kde-format
msgid "Save"
msgstr ""

#: src/Gui/MainToolBarContents.qml:62
#, kde-format
msgid "Save As..."
msgstr ""

#: src/Gui/MainToolBarContents.qml:69
#, kde-format
msgid "Copy"
msgstr ""

#: src/Gui/MainToolBarContents.qml:75
#, kde-format
msgid "Export"
msgstr ""

#: src/Gui/MainToolBarContents.qml:85
#, kde-format
msgid "Show Annotation Tools"
msgstr ""

#: src/Gui/MainToolBarContents.qml:110
#, kde-format
msgid "New Screenshot"
msgstr ""

#: src/Gui/MainToolBarContents.qml:121
#, kde-format
msgid "Options"
msgstr ""

#: src/Gui/MainToolBarContents.qml:130
#, kde-format
msgid "Configure..."
msgstr ""

#: src/Gui/OptionsMenu.cpp:37
#, kde-format
msgid "Capture Mode"
msgstr ""

#: src/Gui/OptionsMenu.cpp:68
#, kde-format
msgid "Show the mouse cursor in the screenshot image"
msgstr ""

#: src/Gui/OptionsMenu.cpp:80
#, kde-format
msgid ""
"Show the window title bar, the minimize/maximize/close buttons, and the "
"window border"
msgstr ""

#: src/Gui/OptionsMenu.cpp:93
#, kde-format
msgid ""
"Capture only the current pop-up window (like a menu, tooltip etc).\n"
"If disabled, the pop-up is captured along with the parent window"
msgstr ""

#: src/Gui/OptionsMenu.cpp:106
#, kde-format
msgid "Quit Spectacle after manually saving or copying the image"
msgstr ""

#: src/Gui/OptionsMenu.cpp:120
#, kde-format
msgid "Capture On Click"
msgstr ""

#: src/Gui/OptionsMenu.cpp:140
#, kde-format
msgid "No Delay"
msgstr ""

#: src/Gui/RecordingView.qml:65
#, kde-format
msgid ""
"Recording:\n"
"%1"
msgstr ""

#: src/Gui/RecordingView.qml:96
#, kde-format
msgid "Pause"
msgstr ""

#: src/Gui/RecordingView.qml:96
#, kde-format
msgid "Play"
msgstr ""

#: src/Gui/RecordOptions.qml:34
#, kde-format
msgid "Recording Settings"
msgstr ""

#: src/Gui/RecordOptions.qml:43
#, kde-format
msgid "Show the mouse cursor in the screen recording."
msgstr ""

#: src/Gui/RecordOptions.qml:53
#, kde-format
msgctxt "@label:listbox"
msgid "Video format:"
msgstr ""

#: src/Gui/RecordOptions.qml:75
#, kde-format
msgid "Finish recording"
msgstr ""

#: src/Gui/SavedAndCopiedMessage.qml:8
#, kde-format
msgid ""
"The screenshot was copied to the clipboard and saved as <a href=\"%1\">%2</a>"
msgstr ""

#: src/Gui/SavedAndLocationCopied.qml:8
#, kde-format
msgid ""
"The screenshot has been saved as <a href=\"%1\">%2</a> and its location has "
"been copied to clipboard"
msgstr ""

#: src/Gui/SavedMessage.qml:14
#, kde-format
msgid "The video was saved as <a href=\"%1\">%2</a>"
msgstr ""

#: src/Gui/SavedMessage.qml:16
#, kde-format
msgid "The screenshot was saved as <a href=\"%1\">%2</a>"
msgstr ""

#: src/Gui/SavedMessage.qml:22
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: src/Gui/ScreenshotFailedMessage.qml:12
#, kde-format
msgid ""
"Could not take a screenshot. Please report this bug here: <a href=\"https://"
"bugs.kde.org/enter_bug.cgi?product=Spectacle\">create a spectacle bug</a>"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, onLaunchSpectacleLabel)
#: src/Gui/SettingsDialog/GeneralOptions.ui:17
#, kde-format
msgid "When launching Spectacle:"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_launchAction)
#: src/Gui/SettingsDialog/GeneralOptions.ui:25
#, kde-format
msgid "Take full screen screenshot"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_launchAction)
#: src/Gui/SettingsDialog/GeneralOptions.ui:30
#, kde-format
msgid "Use last-used capture mode"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_launchAction)
#: src/Gui/SettingsDialog/GeneralOptions.ui:35
#, kde-format
msgid "Do not take a screenshot automatically"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, afterScreenshotLabel)
#: src/Gui/SettingsDialog/GeneralOptions.ui:59
#, kde-format
msgid "After taking a screenshot:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_autoSaveImage)
#: src/Gui/SettingsDialog/GeneralOptions.ui:66
#, kde-format
msgid "Save file to default folder"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_clipboardGroup)
#: src/Gui/SettingsDialog/GeneralOptions.ui:77
#, kde-format
msgid "Do not copy anything"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_clipboardGroup)
#: src/Gui/SettingsDialog/GeneralOptions.ui:82
#, kde-format
msgid "Copy image to clipboard"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_clipboardGroup)
#: src/Gui/SettingsDialog/GeneralOptions.ui:87
#, kde-format
msgid "Copy file location to clipboard"
msgstr ""

#. i18n: ectx: property (text), widget (KTitleWidget, runningTitle)
#: src/Gui/SettingsDialog/GeneralOptions.ui:111
#, kde-format
msgid "While Spectacle is running"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, printKeyActionLabel)
#: src/Gui/SettingsDialog/GeneralOptions.ui:118
#, kde-format
msgid "Press screenshot key to:"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_printKeyActionRunning)
#: src/Gui/SettingsDialog/GeneralOptions.ui:126
#, kde-format
msgid "Take a new Screenshot"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_printKeyActionRunning)
#: src/Gui/SettingsDialog/GeneralOptions.ui:131
#, kde-format
msgid "Open a new Spectacle window"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_printKeyActionRunning)
#: src/Gui/SettingsDialog/GeneralOptions.ui:136
#, kde-format
msgid "Return focus to Spectacle"
msgstr ""

#. i18n: ectx: property (text), widget (KTitleWidget, regionTitle)
#: src/Gui/SettingsDialog/GeneralOptions.ui:160
#, kde-format
msgid "Rectangular Region Selection"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, generalLabel)
#: src/Gui/SettingsDialog/GeneralOptions.ui:167
#, kde-format
msgid "General:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_useLightMaskColor)
#: src/Gui/SettingsDialog/GeneralOptions.ui:174
#, kde-format
msgid "Use light background"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_showMagnifier)
#: src/Gui/SettingsDialog/GeneralOptions.ui:181
#, kde-format
msgid "Show magnifier"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_useReleaseToCapture)
#: src/Gui/SettingsDialog/GeneralOptions.ui:188
#, kde-format
msgid "Accept on click-and-release"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_showCaptureInstructions)
#: src/Gui/SettingsDialog/GeneralOptions.ui:195
#, kde-format
msgid "Show capture instructions"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, rememberLabel)
#: src/Gui/SettingsDialog/GeneralOptions.ui:218
#, kde-format
msgid "Remember selected area:"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_rememberLastRectangularRegion)
#: src/Gui/SettingsDialog/GeneralOptions.ui:226
#, kde-format
msgid "Never"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_rememberLastRectangularRegion)
#: src/Gui/SettingsDialog/GeneralOptions.ui:231
#, kde-format
msgid "Always"
msgstr ""

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_rememberLastRectangularRegion)
#: src/Gui/SettingsDialog/GeneralOptions.ui:236
#, kde-format
msgid "Until Spectacle is closed"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, saveLocationLabel)
#: src/Gui/SettingsDialog/SaveOptions.ui:17
#, kde-format
msgid "Save Location:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, qualityLabel)
#: src/Gui/SettingsDialog/SaveOptions.ui:50
#, kde-format
msgid "Compression Quality:"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, qualitySpinner)
#: src/Gui/SettingsDialog/SaveOptions.ui:74
#, no-c-format, kde-format
msgid "%"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, filenameLabel)
#: src/Gui/SettingsDialog/SaveOptions.ui:88
#, kde-format
msgid "Filename:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, PreviewLabel)
#: src/Gui/SettingsDialog/SaveOptions.ui:133
#, kde-format
msgctxt "Preview of the user configured filename"
msgid "Preview:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, compressionQualityHelpLable)
#: src/Gui/SettingsDialog/SaveOptions.ui:173
#, kde-format
msgid "For lossy image formats like JPEG"
msgstr ""

#: src/Gui/SettingsDialog/SaveOptionsPage.cpp:56
#, kde-format
msgid ""
"You can use the following placeholders in the filename, which will be "
"replaced with actual text when the file is saved:<blockquote>"
msgstr ""

#: src/Gui/SettingsDialog/SaveOptionsPage.cpp:61
#, kde-format
msgid "To save to a sub-folder"
msgstr ""

#: src/Gui/SettingsDialog/SaveOptionsPage.cpp:84
#, kde-kuit-format
msgctxt "@info"
msgid "<filename>%1.%2</filename>"
msgstr ""

#: src/Gui/SettingsDialog/SettingsDialog.cpp:25
#, kde-format
msgid "General"
msgstr ""

#: src/Gui/SettingsDialog/SettingsDialog.cpp:27
#: src/Gui/SettingsDialog/SettingsDialog.cpp:56
#, kde-format
msgid "Shortcuts"
msgstr ""

#. i18n: ectx: label, entry (launchAction), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:15
#, kde-format
msgid "What to do when Spectacle is launched"
msgstr ""

#. i18n: ectx: label, entry (printKeyActionRunning), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:24
#, kde-format
msgid ""
"What should happen if print key is pressed when Spectacle is already running"
msgstr ""

#. i18n: ectx: label, entry (autoSaveImage), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:33
#, kde-format
msgid "Save screenshot automatically after it is taken"
msgstr ""

#. i18n: ectx: label, entry (clipboardGroup), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:37
#, kde-format
msgid "Clipboard action which should be executed after the screenshot is taken"
msgstr ""

#. i18n: ectx: label, entry (useLightMaskColor), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:48
#, kde-format
msgid "Whether to use a light color mask in the region selection dialog"
msgstr ""

#. i18n: ectx: label, entry (showMagnifier), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:52
#, kde-format
msgid "Whether to show a magnifier in the region selection"
msgstr ""

#. i18n: ectx: label, entry (useReleaseToCapture), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:56
#, kde-format
msgid ""
"Whether the screenshot should be captured after selecting the region and "
"releasing the mouse"
msgstr ""

#. i18n: ectx: label, entry (showCaptureInstructions), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:60
#, kde-format
msgid "Whether to show capture instructions in the region selection"
msgstr ""

#. i18n: ectx: label, entry (rememberLastRectangularRegion), group (General)
#: src/Gui/SettingsDialog/spectacle.kcfg:64
#, kde-format
msgid "Remember the last rectangular region"
msgstr ""

#. i18n: ectx: label, entry (cropRegion), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:75
#, kde-format
msgid "The last used region the user selected"
msgstr ""

#. i18n: ectx: label, entry (captureOnClick), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:81
#, kde-format
msgid "Take screenshot on click"
msgstr ""

#. i18n: ectx: label, entry (includePointer), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:85
#, kde-format
msgid "Whether the mouse cursor is included in the screenshot"
msgstr ""

#. i18n: ectx: label, entry (includeDecorations), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:89
#, kde-format
msgid "Whether the window decorations are included in the screenshot"
msgstr ""

#. i18n: ectx: label, entry (transientOnly), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:93
#, kde-format
msgid "Only capture the current pop up menu"
msgstr ""

#. i18n: ectx: label, entry (quitAfterSaveCopyExport), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:97
#, kde-format
msgid "Quit after saving or copying an image"
msgstr ""

#. i18n: ectx: label, entry (captureDelay), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:101
#, kde-format
msgid "Delay"
msgstr ""

#. i18n: ectx: label, entry (videoFormat), group (GuiConfig)
#: src/Gui/SettingsDialog/spectacle.kcfg:110
#, kde-format
msgid "File format for saved screen recordings"
msgstr ""

#. i18n: ectx: label, entry (defaultSaveLocation), group (Save)
#. i18n: ectx: label, entry (defaultVideoSaveLocation), group (Save)
#: src/Gui/SettingsDialog/spectacle.kcfg:116
#: src/Gui/SettingsDialog/spectacle.kcfg:122
#, kde-format
msgid "Default filename"
msgstr ""

#. i18n: ectx: label, entry (compressionQuality), group (Save)
#: src/Gui/SettingsDialog/spectacle.kcfg:128
#, kde-format
msgid "Compression quality for lossy file formats"
msgstr ""

#. i18n: ectx: label, entry (defaultSaveImageFormat), group (Save)
#. i18n: ectx: label, entry (defaultSaveVideoFormat), group (Save)
#: src/Gui/SettingsDialog/spectacle.kcfg:134
#: src/Gui/SettingsDialog/spectacle.kcfg:138
#, kde-format
msgid "Default save image format"
msgstr ""

#. i18n: ectx: label, entry (saveFilenameFormat), group (Save)
#: src/Gui/SettingsDialog/spectacle.kcfg:142
#, kde-format
msgid "The default filename used when saving"
msgstr ""

#. i18n: ectx: label, entry (saveVideoFormat), group (Save)
#: src/Gui/SettingsDialog/spectacle.kcfg:146
#, kde-format
msgid "The default filename used when recording a video"
msgstr ""

#. i18n: ectx: label, entry (lastSaveLocation), group (Save)
#: src/Gui/SettingsDialog/spectacle.kcfg:150
#, kde-format
msgid "The path of the file saved last"
msgstr ""

#. i18n: ectx: label, entry (lastSaveAsLocation), group (Save)
#: src/Gui/SettingsDialog/spectacle.kcfg:154
#, kde-format
msgid "Last path used for \"save as\" action"
msgstr ""

#. i18n: ectx: label, entry (annotationToolType), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:160
#, kde-format
msgid "The last used annotation tool type"
msgstr ""

#. i18n: ectx: label, entry (freehandStrokeWidth), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:166
#, kde-format
msgid "Stroke width for freehand annotation tool"
msgstr ""

#. i18n: ectx: label, entry (highlighterStrokeWidth), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:171
#, kde-format
msgid "Stroke width for highlighter annotation tool"
msgstr ""

#. i18n: ectx: label, entry (lineStrokeWidth), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:176
#, kde-format
msgid "Stroke width for line annotation tool"
msgstr ""

#. i18n: ectx: label, entry (arrowStrokeWidth), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:181
#, kde-format
msgid "Stroke width for arrow annotation tool"
msgstr ""

#. i18n: ectx: label, entry (rectangleStrokeWidth), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:186
#, kde-format
msgid "Stroke width for rectangle annotation tool"
msgstr ""

#. i18n: ectx: label, entry (ellipseStrokeWidth), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:191
#, kde-format
msgid "Stroke width for ellipse annotation tool"
msgstr ""

#. i18n: ectx: label, entry (freehandStrokeColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:197
#, kde-format
msgid "Stroke color for freehand annotation tool"
msgstr ""

#. i18n: ectx: label, entry (highlighterStrokeColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:201
#, kde-format
msgid "Stroke color for highlighter annotation tool"
msgstr ""

#. i18n: ectx: label, entry (lineStrokeColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:205
#, kde-format
msgid "Stroke color for line annotation tool"
msgstr ""

#. i18n: ectx: label, entry (arrowStrokeColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:209
#, kde-format
msgid "Stroke color for arrow annotation tool"
msgstr ""

#. i18n: ectx: label, entry (rectangleStrokeColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:213
#, kde-format
msgid "Stroke color for rectangle annotation tool"
msgstr ""

#. i18n: ectx: label, entry (ellipseStrokeColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:217
#, kde-format
msgid "Stroke color for ellipse annotation tool"
msgstr ""

#. i18n: ectx: label, entry (rectangleFillColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:222
#, kde-format
msgid "Fill color for rectangle annotation tool"
msgstr ""

#. i18n: ectx: label, entry (ellipseFillColor), group (Annotations)
#. i18n: ectx: label, entry (numberFillColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:226
#: src/Gui/SettingsDialog/spectacle.kcfg:230
#, kde-format
msgid "Fill color for ellipse annotation tool"
msgstr ""

#. i18n: ectx: label, entry (textFont), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:235
#, kde-format
msgid "Font for text annotations"
msgstr ""

#. i18n: ectx: label, entry (numberFont), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:238
#, kde-format
msgid "Font for number annotations"
msgstr ""

#. i18n: ectx: label, entry (textFontColor), group (Annotations)
#. i18n: ectx: label, entry (numberFontColor), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:242
#: src/Gui/SettingsDialog/spectacle.kcfg:246
#, kde-format
msgid "Font color for annotations"
msgstr ""

#. i18n: ectx: label, entry (freehandShadow), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:251
#, kde-format
msgid "Whether freehand annotation tool has a drop shadow"
msgstr ""

#. i18n: ectx: label, entry (lineShadow), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:255
#, kde-format
msgid "Whether line annotation tool has a drop shadow"
msgstr ""

#. i18n: ectx: label, entry (arrowShadow), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:259
#, kde-format
msgid "Whether arrow annotation tool has a drop shadow"
msgstr ""

#. i18n: ectx: label, entry (rectangleShadow), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:263
#, kde-format
msgid "Whether rectangle annotation tool has a drop shadow"
msgstr ""

#. i18n: ectx: label, entry (ellipseShadow), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:267
#, kde-format
msgid "Whether ellipse annotation tool has a drop shadow"
msgstr ""

#. i18n: ectx: label, entry (textShadow), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:271
#, kde-format
msgid "Whether text annotation tool has a drop shadow"
msgstr ""

#. i18n: ectx: label, entry (numberShadow), group (Annotations)
#: src/Gui/SettingsDialog/spectacle.kcfg:275
#, kde-format
msgid "Whether number annotation tool has a drop shadow"
msgstr ""

#: src/Gui/SharedMessage.qml:13
#, kde-format
msgid "Image shared"
msgstr ""

#: src/Gui/SharedMessage.qml:14
#, kde-format
msgid ""
"The shared image link (<a href=\"%1\">%1</a>) has been copied to the "
"clipboard."
msgstr ""

#: src/Gui/ShareErrorMessage.qml:13
#, kde-format
msgid "There was a problem sharing the image: %1"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:13
#, kde-format
msgid "Take Screenshot:"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:15
#, kde-format
msgid "Create new selection rectangle:"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:18
#, kde-format
msgid "Move selection rectangle:"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:21
#, kde-format
msgid "Resize selection rectangle:"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:24
#, kde-format
msgid "Reset selection:"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:26
#, kde-format
msgid "Cancel:"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:31
#, kde-format
msgctxt "Mouse action"
msgid "Release left-click"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:31
#, kde-format
msgctxt "Mouse action"
msgid "Double-click"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:32
#, kde-format
msgctxt "Keyboard action"
msgid "Enter"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:33
#, kde-format
msgctxt "Mouse action"
msgid "Drag outside selection rectangle"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:34
#, kde-format
msgctxt "Keyboard action"
msgid "+ Shift: Magnifier"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:36
#, kde-format
msgctxt "Mouse action"
msgid "Drag inside selection rectangle"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:37
#, kde-format
msgctxt "Keyboard action"
msgid "Arrow keys"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:38
#, kde-format
msgctxt "Keyboard action"
msgid "+ Shift: Move in 1 pixel steps"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:39
#, kde-format
msgctxt "Mouse action"
msgid "Drag handles"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:40
#, kde-format
msgctxt "Keyboard action"
msgid "Arrow keys + Alt"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:41
#, kde-format
msgctxt "Keyboard action"
msgid "+ Shift: Resize in 1 pixel steps"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:42
#, kde-format
msgctxt "Mouse action"
msgid "Right-click"
msgstr ""

#: src/Gui/ShortcutsTextBox.qml:44
#, kde-format
msgctxt "Keyboard action"
msgid "Escape"
msgstr ""

#: src/Gui/SpectacleWindow.cpp:228
#, kde-format
msgctxt "@title:window"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] ""
msgstr[1] ""

#: src/Gui/SpectacleWindow.cpp:231
#, kde-format
msgctxt "@title:window Unsaved Screenshot"
msgid "Unsaved"
msgstr ""

#: src/Gui/UndoRedoGroup.qml:32
#, kde-format
msgid "Undo"
msgstr ""

#: src/Gui/UndoRedoGroup.qml:46
#, kde-format
msgid "Redo"
msgstr ""

#: src/Main.cpp:42
#, kde-format
msgid "Spectacle"
msgstr ""

#: src/Main.cpp:44
#, kde-format
msgid "KDE Screenshot Utility"
msgstr ""

#: src/Main.cpp:46
#, kde-format
msgid "(C) 2015 Boudhayan Gupta"
msgstr ""

#: src/Main.cpp:50
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: src/Main.cpp:50
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "shijualexonline@gmail.com,snalledam@dataone.in,vivekkj2004@gmail.com"

#: src/Main.cpp:66
#, kde-format
msgid ""
"On Wayland, Spectacle requires KDE Plasma's KWin compositor, which does not "
"seem to be available. Use Spectacle on KDE Plasma, or use a different "
"screenshot tool."
msgstr ""

#: src/RecordingModeModel.cpp:30
#, kde-format
msgid "Workspace"
msgstr ""

#: src/RecordingModeModel.cpp:36
#, kde-format
msgid "Selected Screen"
msgstr ""

#: src/RecordingModeModel.cpp:42
#, kde-format
msgid "Selected Window"
msgstr ""

#: src/RecordingModeModel.cpp:99
#, kde-format
msgid "Failed to select output"
msgstr ""

#: src/RecordingModeModel.cpp:130
#, kde-format
msgid "Failed to select window"
msgstr ""

#: src/ShortcutActions.cpp:63
#, kde-format
msgid "Capture Window Under Cursor"
msgstr ""

#: src/ShortcutActions.cpp:69
#, kde-format
msgid "Launch Spectacle without capturing"
msgstr ""

#: src/SpectacleCore.cpp:635
#, kde-format
msgid "Screenshot capture canceled or failed"
msgstr ""

#: src/SpectacleCore.cpp:680
#, kde-format
msgid ""
"A screenshot was saved as '%1' to '%2' and the file path of the screenshot "
"has been saved to your clipboard."
msgstr ""

#: src/SpectacleCore.cpp:684
#, kde-format
msgctxt "Placeholder is filename"
msgid "A screenshot was saved as '%1' to your Pictures folder."
msgstr ""

#: src/SpectacleCore.cpp:687
#, kde-format
msgid "A screenshot was saved as '%1' to '%2'."
msgstr ""

#: src/SpectacleCore.cpp:691
#, kde-format
msgid "A screenshot was saved to your clipboard."
msgstr ""

#: src/SpectacleCore.cpp:696
#, kde-format
msgctxt "Open the screenshot we just saved"
msgid "Open"
msgstr ""

#: src/SpectacleCore.cpp:701
#, kde-format
msgid "Annotate"
msgstr ""
